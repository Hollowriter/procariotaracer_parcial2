﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CelulaAnimator : MonoBehaviour {
	Animator anim;
	void Start () {
		anim = GetComponent<Animator> ();
	}
	void Update () {
		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			anim.SetBool ("isPressed", true);
		} else if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			anim.SetBool ("isPressed", true);
		} else {
			anim.SetBool ("isPressed", false);
		}
	}
}
