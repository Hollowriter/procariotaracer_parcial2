﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePill : MonoBehaviour {
	[SerializeField]
	private Transform m_RaceCenter;
	[SerializeField]
	private float m_SecondsByLap;
	private float m_CurrentSpeed = 1f;
	[SerializeField]
	private float m_SideSpeed = 20f;
	[SerializeField]
	private float m_MaxDistance = 2f;
	[SerializeField]
	private float m_MinDistance = 1f;
	private int m_CurrentLap;
	[SerializeField]
	private int m_TotalLaps;
	[SerializeField]
	private float m_MaxSpeed = 30f;
	[SerializeField]
	private float m_MinSpeed = 1f;
	private float m_SpeedDecrementBySecond = 0.03f;
	private float m_SpeedIncrement = 0.1f;
	private float m_ElapsedTime = 0f;
	private bool m_Running = true;
	void Update () {
		if (!m_Running) {
			return;
		}
		float angles = -360f / m_SecondsByLap;
		transform.RotateAround (m_RaceCenter.position, Vector3.up, angles * Time.deltaTime * m_CurrentSpeed);
		float inputHorizontal = Input.GetAxis ("Horizontal");
		float xtranslation = inputHorizontal * Time.deltaTime * m_SideSpeed;
		float distance = Vector3.Distance (m_RaceCenter.position, transform.position);
		if (inputHorizontal > 0f && distance > m_MinDistance || inputHorizontal < 0f && distance < m_MaxDistance) {
			transform.Translate (new Vector3 (xtranslation, 0f, 0f));
		}
		if (m_CurrentSpeed > m_MinSpeed) {
			m_CurrentSpeed -= m_SpeedDecrementBySecond * Time.deltaTime;
		}
		m_ElapsedTime += Time.deltaTime;
	}
	private void OnTriggerEnter(Collider collider){
		Debug.Log ("enter");
		if (collider.tag == "Check") {
			m_CurrentLap++;
			if (m_CurrentLap >= m_TotalLaps) {
				EndRace ();
			}
		} else if (collider.gameObject.tag == "PickUp") {
			if (m_CurrentSpeed < m_MaxSpeed) {
				m_CurrentSpeed += m_SpeedIncrement;
				Destroy (collider.gameObject);
				Debug.Log (m_CurrentSpeed);
			}
		}
	}
	private void EndRace(){
		Debug.Log ("Total time: " + m_ElapsedTime.ToString());
		m_Running = false;
		Camera mainCamera = Camera.main;
		OrbitScreen orbitCameraComponent = mainCamera.gameObject.AddComponent<OrbitScreen> ();
		orbitCameraComponent.StartOrbit (transform.position);
	}
}
