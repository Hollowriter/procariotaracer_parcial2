﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickedUp : MonoBehaviour {
	[SerializeField]
	private float m_RevolutionTime = 1f;
	[SerializeField]
	private Transform m_PickUpObjectTransform;
	void Update () {
		float angles = 360f / m_RevolutionTime;
		m_PickUpObjectTransform.Rotate (Vector3.up, angles * Time.deltaTime);
	}
}
